import React from 'react'
import Routing from './routing/index'
import Header from '../src/component/layout/header'
import 'antd/dist/antd.css'

function App() {
    return (
        <div className='App'>
            <header className='App-header'>
                <Header />
            </header>
            <Routing />
        </div>
    )
}

export default App
