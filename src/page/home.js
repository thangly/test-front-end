import React from 'react'
import '../styles/home.scss'
import imgChair from '../static/home-product-chair.jpg'
import imgArrow from '../static/home-product-arrow.png'
import imgChairHouse from '../static/home-product-chair-house.jpg'
import imgArrow2 from '../static/home-product-arrow2.png'
import imgProduct01 from '../static/product-01.png'
import imgProductInfo01 from '../static/product-info-01.jpg'

function HomePage() {
    return (
        <main id='home-page'>
            <section className='welcome'>
                <div className='content'>
                    <h2>Hey, I got here by chance</h2>
                    <p>
                        Create 3D models. Enjoy Cloud Based Floor Planner,
                        Panorama 3D, 4K Rendering, 360 HD Viewer and AR Viewer.
                        Boost your Sales with Archisketch
                    </p>
                    <div className='try'>
                        <button>Free Version</button>
                        <button>Enterprise Free Trial</button>
                    </div>
                </div>
            </section>
            <section className='products'>
                <div className='title'>
                    <h2>Products</h2>
                    <p>
                        Deliver Beautiful Photorealistic Images with 3D models
                    </p>
                </div>
                <div className='product-in-house'>
                    <div className='before'>
                        <img src={imgChair} alt='chair' />
                    </div>
                    <div className='arrow'>
                        <img src={imgArrow} alt='arrow' />
                    </div>
                    <div className='after'>
                        <img src={imgChairHouse} alt='chair in house' />
                    </div>
                </div>
                <div className='product-orther'>
                    <div className='arrow-down'>
                        <img src={imgArrow2} alt='arrow' />
                        <p>
                            Click to Receive Five Different Visualizations from
                            One Source
                        </p>
                    </div>
                    <div className='list-products'>
                        <div className='item-product'>
                            <img src={imgProduct01} alt='Floor Planner' />
                            <h4>Floor Planner</h4>
                        </div>
                        <div className='item-product'>
                            <img src={imgProduct01} alt='Floor Planner' />
                            <h4>Floor Planner</h4>
                        </div>
                        <div className='item-product'>
                            <img src={imgProduct01} alt='Floor Planner' />
                            <h4>Floor Planner</h4>
                        </div>
                        <div className='item-product'>
                            <img src={imgProduct01} alt='Floor Planner' />
                            <h4>Floor Planner</h4>
                        </div>
                        <div className='item-product'>
                            <img src={imgProduct01} alt='Floor Planner' />
                            <h4>Floor Planner</h4>
                        </div>
                    </div>
                </div>
                <div className='info-products'>
                    <div className='item-info-product'>
                        <div className='info'>
                            <h2 className='title'>Floor Planner</h2>
                            <h3 className='content-main'>
                                Design your clients home through home planner
                                with a variety of furniture and material in
                                Archisketch library. Make clients imagination
                                come true.
                            </h3>
                            <p>
                                Realize your client’s imagination with the world
                                easiest tool, Floor Planner. It also comes with
                                variety of furniture and finishing materials in
                                Archichketch library
                            </p>
                        </div>
                        <div className='image'>
                            <img src={imgProductInfo01} alt='' />
                        </div>
                    </div>
                    <div className='item-info-product'>
                        <div className='info'>
                            <h2 className='title'>Floor Planner</h2>
                            <h3 className='content-main'>
                                Design your clients home through home planner
                                with a variety of furniture and material in
                                Archisketch library. Make clients imagination
                                come true.
                            </h3>
                            <p>
                                Realize your client’s imagination with the world
                                easiest tool, Floor Planner. It also comes with
                                variety of furniture and finishing materials in
                                Archichketch library
                            </p>
                        </div>
                        <div className='image'>
                            <img src={imgProductInfo01} alt='' />
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}

export default HomePage
