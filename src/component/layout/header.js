import React from 'react'
import '../../styles/header.scss'
import logo from '../../static/logo.jpg'
import { Menu, Dropdown } from 'antd'
import { DownOutlined } from '@ant-design/icons'

const menu = (
    <Menu>
        <Menu.Item key='0'>
            <span>English</span>
        </Menu.Item>
        <Menu.Item key='1'>
            <span>한국어</span>
        </Menu.Item>
    </Menu>
)

function Header() {
    return (
        <div id='header'>
            <div className='logo'>
                <a href='/'>
                    <img src={logo} alt='logo' />
                </a>
            </div>
            <ul className='menu'>
                <li>
                    <a href='/'>Pricing</a>
                </li>
                <li>
                    <a href='/'>Blog</a>
                </li>
                <li>
                    <a href='/'>Forums</a>
                </li>
                <li>
                    <a href='/'>Help Center</a>
                </li>
                <li>
                    <a href='/'>Lean</a>
                </li>
            </ul>
            <div className='language-start'>
                <Dropdown
                    className='language'
                    overlay={menu}
                    trigger={['click']}
                >
                    <b className='ant-dropdown-link'>
                        English
                        <DownOutlined />
                    </b>
                </Dropdown>
                <button className='start'>Start Your Design</button>
            </div>
        </div>
    )
}

export default Header
